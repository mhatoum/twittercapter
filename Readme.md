- Searching for keywords ( #node app.js search <keywords> ) of from and to a user ( #node app.js search from:@user @user )
- Looking at a stream of data, filtered on keywords ( #node app.js stream <keywords> )
- Lookup a user ( #node app.js lookup <user> )
- Check trends ( #node app.js trends 1 ), for now I included the woeid from the Netherlands as option 2. (A WoeID = Where On Earth ID, created by Yahoo. I found the id using this tool)
- Dump the tweets from a user ( #node app.js dump <user> )

Dumping tweets is fun. You could put them into a database and analyse it. Twitter API allows you to dump  approximately 3200 tweets. In most cases it will not be this amount of tweets, but a lot less (because tweets get deleted, etcetera, if I recall this right).


/*
[statuses][0]

[metadata]: undefined
[created_at]: undefined
[id]: undefined
[id_str]: undefined
[text]: undefined
[source]: undefined
[truncated]: undefined
[in_reply_to_status_id]: undefined
[in_reply_to_status_id_str]: undefined
[in_reply_to_user_id]: undefined
[in_reply_to_user_id_str]: undefined
[in_reply_to_screen_name]: undefined
[user]: undefined
[geo]: undefined
[coordinates]: undefined
[place]: undefined
[contributors]: undefined
[retweet_count]: undefined
[favorite_count]: undefined
[entities]: undefined
[favorited]: undefined
[retweeted]: undefined
[lang]: undefined
*/

/*
[user]

[id]: undefined
[id_str]: undefined
[name]: undefined
[screen_name]: undefined
[location]: undefined
[description]: undefined
[url]: undefined
[entities]: undefined
[protected]: undefined
[followers_count]: undefined
[friends_count]: undefined
[listed_count]: undefined
[created_at]: undefined
[favourites_count]: undefined
[utc_offset]: undefined
[time_zone]: undefined
[geo_enabled]: undefined
[verified]: undefined
[statuses_count]: undefined
[lang]: undefined
[contributors_enabled]: undefined
[is_translator]: undefined
[profile_background_color]: undefined
[profile_background_image_url]: undefined
[profile_background_image_url_https]: undefined
[profile_background_tile]: undefined
[profile_image_url]: undefined
[profile_image_url_https]: undefined
[profile_banner_url]: undefined
[profile_link_color]: undefined
[profile_sidebar_border_color]: undefined
[profile_sidebar_fill_color]: undefined
[profile_text_color]: undefined
[profile_use_background_image]: undefined
[default_profile]: undefined
[default_profile_image]: undefined
[following]: undefined
[follow_request_sent]: undefined
[notifications]: undefined
*/