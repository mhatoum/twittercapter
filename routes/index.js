var twitter = require('../js/twitter');

exports.home = function(request, response) {
	console.log(' index/home');
	response.render('index');
}

exports.search = function(request, response) {
	console.log(' index/search');

	twitter.getSearchTweets(request.body.search, request.body.count, function(myReturnedResponse) {
		response.render('result', { tweets: myReturnedResponse });	
	});
}

exports.stream = function(request, response) {
	console.log(' index/stream')

	twitter.streamSearchTweets(request.body.search, function() {
	})
}

exports.trend = function(request, response) {
	console.log(' index/trend')
}