// Module dependencies
var express = require('express'),
	connect = require('connect'),
	path = require('path'),
	http = require('http');

var index = require('./routes/index');

// App declaration
var app = express();
var server = http.createServer(app);
var io = require('socket.io').listen(server);

/*
var app = require('express')()
  , server = require('http').createServer(app)
  , io = require('socket.io').listen(server);

server.listen(80);

app.get('/', function (req, res) {
  res.sendfile(__dirname + '/index.html');
});

io.sockets.on('connection', function (socket) {
  socket.emit('news', { hello: 'world' });
  socket.on('my other event', function (data) {
    console.log(data);
  });
});
*/



// App USE
app.use(express.favicon());
app.use(express.logger('dev'));
app.use(connect.urlencoded())
app.use(connect.json())
app.use(express.methodOverride());
app.use(app.router);
app.use(express.static(path.join(__dirname, 'public')));

// App SET
app.set('port', process.env.PORT || 3000);
app.set('views', __dirname + '/views');
app.set('view engine', 'jade');

//
app.get('/', index.home);

app.post('/search', index.search);

// Server creation
server.listen(app.get('port'), function(){
	console.log(' Server is launch...');
});
// Listen Socket.io
io.sockets.on('connection', function (socket) {
	socket.emit('news', { hello: 'world' });

	socket.on('my other event', function (data) {
		console.log(data);
	});
});